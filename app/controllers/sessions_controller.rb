class SessionsController < ApplicationController
    def create
      user = User.find_by(email: params[:email])
      
      if user && user.authenticate(params[:password])
        jwt_token = encode_token(user_id: user.id)
        render json: { jwt_token: jwt_token }
      else
        render json: { error: "Correo electrónico o contraseña incorrectos." }, status: :unauthorized
      end
    end
  
    private
  
    def encode_token(payload)
      secret_key = 'my_secret_key'
      JWT.encode(payload, secret_key, 'HS256')
    end
  end
  
  