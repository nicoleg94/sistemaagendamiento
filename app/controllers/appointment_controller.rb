class AppointmentController < ApplicationController
    
    def available_hours
        date = Date.parse(params[:date])
        day_of_week = date.strftime('%A')
        puts day_of_week
        available_hours = case day_of_week
                          when 'Saturday'
                            ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00']
                          when 'Sunday'
                            []
                          else
                            ['16:00', '17:00', '18:00', '19:00', '20:00']
                          end
    
        existing_appointments = Appointment.where(date: date)
        existing_hours = existing_appointments.map { |appointment| appointment.time.strftime('%H:%M') }
    
        available_hours = available_hours.select { |hour| !existing_hours.include?(hour) }
    
        render json: { available_hours: available_hours }
    end
    
    
    def create
      @patient = Patient.find_or_create_by(idCard: params[:idCard]) do |patient|
        patient.name = params[:name]
        patient.age = params[:age]
        patient.representativeName = params[:representativeName]
        patient.contactPhone = params[:contactPhone]
        patient.contactEmail = params[:contactEmail]
        patient.contactEmail2 = params[:contactEmail2]
      end
  
      appointment_date = Date.parse(params[:selectedDate])
      appointment_time = Time.parse(params[:selectedHour]).strftime('%H:%M')
      @appointment = @patient.appointments.build(date: appointment_date, time: appointment_time)
  
      if @appointment.save
        render json: @appointment, status: :created
      else
        render json: { errors: @appointment.errors.full_messages }, status: :unprocessable_entity
      end
    end

    def get_by_date
      date = Date.parse(params[:date])
      appointments = Appointment.where(date: date).map do |appointment|
        {
          id: appointment.id,
          date: appointment.date.strftime('%Y-%m-%d'),
          time: appointment.time.strftime('%H:%M'),
          patient: {
            id: appointment.patient.id,
            name: appointment.patient.name,
            age: appointment.patient.age,
            representativeName: appointment.patient.representativeName,
            contactPhone: appointment.patient.contactPhone,
            contactEmail: appointment.patient.contactEmail,
            contactEmail2: appointment.patient.contactEmail2
          }
        }
      end
    
      render json: appointments
    end

    def update
      @appointment = Appointment.find(params[:id])
      @appointment.date = Date.parse(params[:date])
      @appointment.time = Time.parse(params[:time]).strftime('%H:%M')
      @appointment.save
      render json: @appointment
    end

    def destroy
      @appointment = Appointment.find(params[:id])
      @appointment.destroy
      render json: @appointment
    end
end