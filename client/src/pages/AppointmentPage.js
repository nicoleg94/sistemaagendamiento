import React, { useState } from "react";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import HourPicker from "../components/HourPicker/HourPicker";
import { useAppointment } from "../contexts/AppointmentContext";
import dayjs from "dayjs";


const AppointmentPage = () => {
  const [open, setOpen] = useState(false);
  const tomorrow = dayjs().add(1, 'day');
  const { selectedDate, setSelectedDate } = useAppointment();

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
       day: "2-digit",
      month: "short",
      year: "numeric",
    };
    return date.toLocaleDateString("es-ES", options);
  };

  const handleDateChange = (newDate) => {
    const formattedDate = formatDate(newDate.$d);
    setSelectedDate(formattedDate);
    setOpen(true);
  };


  return (
    <div className="appointment-container">
      <div className="main-section">
        <div className="main-section-title">
          <h1>Agenda una cita</h1>
        </div>
        <div className="main-section-calendar">
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateCalendar onChange={handleDateChange} minDate={tomorrow}/>
          </LocalizationProvider>
        </div>
        {open && <HourPicker selectedDate={selectedDate} />}
      </div>
    </div>
  );
};

export default AppointmentPage;
