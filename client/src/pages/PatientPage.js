import React, { useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from "@mui/material";

import { useAppointment } from "../contexts/AppointmentContext";
import { Navigate, useNavigate } from "react-router-dom";	

const PatientPage = () => {
  const {
    selectedDate,
    selectedHour,
    patientData,
    setPatientData,
    sendAppointmentDataToBackend,
  } = useAppointment();

  const [showDialog, setShowDialog] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const navigate = useNavigate();

  const handleChange = (e) => {
    setPatientData({
      ...patientData,
      [e.target.name]: e.target.value,
    });
  };

  const isFormValid = () => {
    return (
      patientData.idCard.trim() !== "" &&
      patientData.name.trim() !== "" &&
      patientData.representativeName.trim() !== "" &&
      patientData.contactPhone.trim() !== "" &&
      patientData.contactEmail.trim() !== "" &&
      selectedDate !== null &&
      selectedHour !== null
    );
  };

  const handleSubmit = async () => {
    if (!isFormValid()) {
      setAlertMessage("Por favor completa todos los campos obligatorios.");
      setShowDialog(true);
      return;
    }

    try {
      await sendAppointmentDataToBackend();
      setAlertMessage("¡La cita ha sido programada exitosamente!");
      setShowDialog(true);
    } catch (error) {
      setAlertMessage(
        "Hubo un problema al programar la cita. Por favor, intenta de nuevo."
      );
      setShowDialog(true);
      console.error("Error al enviar los datos de la cita al backend:", error);
    }
  };

  const handleDialogClose = () => {
    if (alertMessage === "¡La cita ha sido programada exitosamente!") {
      navigate("/");
    }
    else {
      setShowDialog(false);
    }
  };

  return (
    <div className="home-container-pat">
      <div className="main-section-pat">
        <div className="main-section-left-pat">
          <h1>Datos del paciente</h1>
          <p>Cédula de la o el paciente:</p>
          <TextField
            required
            id="outlined-required"
            label="Requerido"
            name="idCard"
            onChange={handleChange}
            value={patientData.idCard}
          />
          <p>Nombre de la o el paciente:</p>
          <TextField
            required
            id="outlined-required"
            label="Requerido"
            name="name"
            onChange={handleChange}
            value={patientData.name}
          />
          <p>Edad de la o el paciente:</p>
          <TextField
            id="outlined-number"
            label="Requerido"
            type="number"
            name="age"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleChange}
            value={patientData.age}
          />
          <p>Nombre de la o el representante de la o el paciente:</p>
          <TextField
            required
            id="outlined-required"
            label="Requerido"
            name="representativeName"
            onChange={handleChange}
            value={patientData.representativeName}
          />
          <p>Teléfono de contacto:</p>
          <TextField
            required
            id="outlined-required"
            label="Requerido"
            name="contactPhone"
            onChange={handleChange}
            value={patientData.contactPhone}
          />
          <p>Correo electrónico de contacto:</p>
          <TextField
            required
            id="outlined-required"
            label="Requerido"
            name="contactEmail"
            onChange={handleChange}
            value={patientData.contactEmail}
          />
          <p>Correo electrónico de contacto adicional:</p>
          <TextField
            id="outlined-basic"
            name="contactEmail2"
            onChange={handleChange}
            value={patientData.contactEmail2}
          />
          <p>
            Esta cita se agendará para {selectedDate} a las {selectedHour}.
          </p>
          <br />
          <br />
          <Button variant="contained" onClick={handleSubmit}>
            Confirmar la cita
          </Button>
        </div>

        <Dialog
          open={showDialog}
          onClose={handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {alertMessage}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDialogClose} autoFocus>
              Cerrar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
};

export default PatientPage;
