import React, { useState, useEffect } from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar';
import { useAdmin } from '../contexts/AdminContext';
import AppointmentDetails from '../components/AppointmentList/AppointmentList';
import './AdminPageCss.css';
import { Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import HomeIcon from '@mui/icons-material/Home';

const AdminPage = () => {
    const { selectedDate, setSelectedDate, getAppointmentsByDate } = useAdmin(); 
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (selectedDate) {
            getAppointmentsByDate(selectedDate);
            setOpen(true);
        }
    }, [selectedDate]);

    const formatDate = (dateString) => {
        const date = new Date(dateString);
        const options = {
            day: "2-digit",
            month: "short",
            year: "numeric",
        };
        return date.toLocaleDateString("es-ES", options);
    };
    
    const handleDateChange = (newDate) => {
        const formattedDate = formatDate(newDate.$d);
        setSelectedDate(formattedDate);
    };


    return (
        <div className="appointment-container">
            <div className="home-button">
            
            <Link to="/">
            <Button>
                    <HomeIcon/>
            </Button>
            </Link>
            </div>
            <div className="main-section">
                <div className="main-section-title">
                    <h1>CITAS PROGRAMADAS</h1>
                    <p>Selecciona la fecha de consulta:</p>
                </div>
                <div className="main-section-calendar">
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DateCalendar onChange={handleDateChange}/>
                    </LocalizationProvider>
                </div>
                <div className="main-section-appointment">
                {open && <AppointmentDetails />}
                </div>
            </div>
        </div>
    );
};

export default AdminPage;