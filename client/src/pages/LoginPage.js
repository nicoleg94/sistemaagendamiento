import React, { useState } from 'react';
import { Container, Typography, TextField, Button, Grid } from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './LoginPageCss.css';

const LoginPage = () => {

    const history = useNavigate();

    const [formData, setFormData] = useState({
        email: '',
        password: ''
      });

  
    const handleChange = (e) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
    
        try {
          const response = await axios.post('http://localhost:3001/login', formData);
          const { jwt_token } = response.data;
    
          localStorage.setItem('token', jwt_token);
    
          history('/administracion');
      
        } catch (error) {
          console.error('Error al iniciar sesión:', error);
        }
      };


    return (
        <div className="login-page">
        <Container component="main" maxWidth="xs" className="login-page-root">
          <Typography component="h1" variant="h5" align="center">
            Iniciar sesión
          </Typography>
          <form className="login-form" onSubmit={"handleSubmit"}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Correo electrónico"
              name="email"
              autoFocus
              onChange={handleChange}
              value={formData.email}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Contraseña"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={handleChange}
              value={formData.password}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className="login-submit"
              onClick={handleSubmit}
            >
              Ingresar
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <p>Si tienes problema para iniciar sesión, contáctate con soporte.</p>
              </Grid>
            </Grid>
          </form>
        </Container>
      </div>
      );
    };

export default LoginPage;
