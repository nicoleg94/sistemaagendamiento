import React from 'react';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import './HomePageCss.css';
import logo from '../assets/logo.svg';
import contactInfo from '../assets/contactInfo.png';

const HomePage = () => {
    return (
        <div className="home-page">
          <div className="header">
            <div className="header-logo">
            <Link to="/login">
                <img className="logo" src={logo}></img>
                </Link>
            </div>
            <div className="header-title">
                <img className="contactInfo" src={contactInfo}></img>

            </div>
          </div>
          <div className="main-section-home">
            <div className="main-section-img">
              <img src="https://cc834f82.rocketcdn.me/wp-content/uploads/2023/07/shutterstock_1869308083.webp" alt="main-section-img" />
            </div>
            <div className="main-section-right">
              <h2>La salud de tus pequeños en las mejores manos</h2>
              <p>Protege a tu hija o hijo ahora.</p>
              <Link to="/agendamiento">
                <Button variant="contained" color="secondary">Agendar una cita</Button>
              </Link>
            </div>
          </div>
          <div className="footer-container">
          </div>
        </div>
      );
    };
    
    export default HomePage;

