import './App.css';
import HomePage from './pages/HomePage';
import AppointmentPage from './pages/AppointmentPage';
import PatientPage from './pages/PatientPage';
import LoginPage from './pages/LoginPage';
import { BrowserRouter, Routes, Route,Navigate  } from 'react-router-dom';
import AdminPage from './pages/AdminPage';


function App() {
  
  return (
    <div className="App">
      <BrowserRouter>      
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/agendamiento" element={<AppointmentPage />} />
          <Route path="/paciente" element={<PatientPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="*" element={<Navigate to="/" />} />
          <Route path="/administracion" element={<AdminPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
