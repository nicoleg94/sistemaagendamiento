import React, { useState, useEffect } from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import { useAppointment } from "../../contexts/AppointmentContext";

const HourPicker = () => {
  const [availableHours, setAvailableHours] = useState([]);

  const { selectedDate, selectedHour, setSelectedHour } = useAppointment();
  const { getAvailableHours } = useAppointment();
  
  useEffect(() => {
    const fetchAvailableHours = async () => {
      const hours = await getAvailableHours(selectedDate);
      setAvailableHours(hours);
    };

    fetchAvailableHours();
  }, [selectedDate, getAvailableHours]);

  const handleHourChange = (event) => {
    setSelectedHour(event.target.value);
  };

  const handleAppointmentBooking = () => {
    console.log(selectedHour);
    console.log(selectedDate);
  };

  return (
    <div>
      <h2>Selecciona la hora de la cita</h2>
      <p>Se muestran solo los horarios disponibles</p>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        name="radio-buttons-group"
        value={selectedHour}
        onChange={handleHourChange}
      >
        {availableHours.map((hourValue) => (
          <FormControlLabel
            key={hourValue}
            value={hourValue}
            control={<Radio />}
            label={hourValue}
          />
        ))}
      </RadioGroup>
      <Link
        to={{ pathname: "/paciente" }}
      >
        <Button variant="contained" onClick={handleAppointmentBooking}>
          Agendar
        </Button>
      </Link>
    </div>
  );
};

export default HourPicker;
