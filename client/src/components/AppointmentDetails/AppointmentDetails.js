import React, {useState} from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useAdmin } from '../../contexts/AdminContext';
import TextField from "@mui/material/TextField";
import './AppointmentDetailsCss.css';

const AppointmentDetails = ({ appointment, handleClose }) => {
    const { getAppointmentsByDate, updateAppointment, deleteAppointment } = useAdmin();
    const [updatedAppointment, setUpdatedAppointment] = useState(appointment);

  
    const handleUpdate = async () => {
      await updateAppointment(updatedAppointment.id, updatedAppointment);
      await getAppointmentsByDate(updatedAppointment.date);
      handleClose();
    };
  
    const handleDelete = async () => {
      await deleteAppointment(updatedAppointment.id);
      await getAppointmentsByDate(updatedAppointment.date);
      handleClose();
    };
  
    const handleChange = (e) => {
      const { name, value } = e.target;
      setUpdatedAppointment(prevState => ({
        ...prevState,
        [name]: value
      }));
    };
  
    return (
      <div>
        <Box className="box">
          <Typography id="modal-modal-title" className="title-modal">
            <h2>Detalles de la cita</h2>
          </Typography>

          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <TextField
              className="input-date"
              name="date"
              label="Fecha"
              type="date"
              value={updatedAppointment.date}
              onChange={handleChange}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              name="time"
              label="Hora"
              type="time"
              value={updatedAppointment.time}
              onChange={handleChange}
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300,
              }}
            />
            </Typography><Typography>
        <p><b>Nombre de paciente:</b> {appointment.patient.name}</p>
        <p><b>Edad de paciente:</b> {appointment.patient.age}</p>
        <p><b>Representante de paciente:</b> {appointment.patient.representativeName}</p>
        <p><b>Teléfono de contacto:</b> {appointment.patient.contactPhone}</p>
        <p><b>Correo de contacto:</b> {appointment.patient.contactEmail}</p>
        <p><b>Correo de contacto adicional:</b> {appointment.patient.contactEmail2}</p>
        </Typography>
        <Button variant="contained" color="secondary" onClick={handleUpdate}>Guardar Cambios</Button>
        <Button className="delete-button" variant="contained" color="error" onClick={handleDelete}>Eliminar Cita</Button>
      </Box>
    </div>
  );
};

export default AppointmentDetails;