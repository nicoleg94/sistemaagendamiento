import React, { useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import VisibilityIcon from '@mui/icons-material/Visibility';
import IconButton from '@mui/material/IconButton';
import Modal from '@mui/material/Modal';
import AppointmentDetails from "../AppointmentDetails/AppointmentDetails";
import { useAdmin } from '../../contexts/AdminContext';
import { Snackbar } from "@mui/material";
import './AppointmentListCss.css';

const AppointmentList = () => {
  const { selectedDate, appointments, successMessage, setSuccessMessage } = useAdmin();
  const [selectedAppointment, setSelectedAppointment ] = useState(null);
  const [open, setOpen] = useState(false);

  const sortedAppointments = appointments.slice().sort((a, b) => {
    return a.time.localeCompare(b.time);
  });

  const handleOpen = (appointment) => {
    setSelectedAppointment(appointment);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseSnackbar = () => {
    setSuccessMessage('');
  };

  return (
    <div>
      <h2>Citas programadas para {selectedDate}</h2>
      <TableContainer component={Paper} className="table-container">
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className="table-cell-header">Hora</TableCell>
              <TableCell className="table-cell-header">Paciente</TableCell>
              <TableCell sx={{ maxWidth: 650 }} align="right" className="table-cell-header-last">Más información</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedAppointments.map((appointment) => (
              <TableRow key={appointment.id}>
                <TableCell className="table-cell" align="left" component="th" scope="row">
                  {appointment.time}
                </TableCell>
                <TableCell className="table-cell" align="left">{appointment.patient.name}</TableCell>
                <TableCell className="table-cell-last" align="right">
                  <IconButton aria-label="visibility" onClick={() => handleOpen(appointment)}>
                    <VisibilityIcon/>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className="modal">
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <AppointmentDetails appointment={selectedAppointment} handleClose={handleClose} />
      </Modal>
      </div>
      <div className="snackbar">
      <Snackbar
        open={!!successMessage}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
        message={successMessage}
        severity="success">
      </Snackbar>
      </div>
    </div>
  );
};

export default AppointmentList;