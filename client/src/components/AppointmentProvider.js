import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppointmentProvider } from './contexts/appointmentContext';

import App from './App';

ReactDOM.render(
  <Router>
    <AppointmentProvider>
      <App />
    </AppointmentProvider>
  </Router>,
  document.getElementById('root')
);
