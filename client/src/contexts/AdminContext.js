import React, { createContext, useContext, useState } from "react";
import axios from "axios";

const AdminContext = createContext();

export const AdminProvider = ({ children }) => {
  const [selectedDate, setSelectedDate] = useState();
  const [appointments, setAppointments] = useState([]);
  const [successMessage, setSuccessMessage] = useState('');

  const getAppointmentsByDate = async (date) => {
    try {
      const response = await axios.get(
        `http://localhost:3001/appointment/date?date=${date}`
      );
      setAppointments(response.data);
    } catch (error) {
      console.error("Error al obtener las citas por fecha:", error);
    }
  };

  const updateAppointment = async (id, updatedAppointment) => {
    try {
      await axios.put(
        `http://localhost:3001/appointment/update?id=${id}`,
        updatedAppointment
      );
      setSuccessMessage('Cita médica actualizada correctamente.');
    } catch (error) {
      console.error("Error al actualizar la cita médica:", error);
    }
  };

  const deleteAppointment = async (id) => {
    try {
      await axios.delete(`http://localhost:3001/appointment?id=${id}`);
      setSuccessMessage('Cita médica eliminada correctamente.');
    } catch (error) {
      console.error("Error al eliminar la cita médica:", error);
    }
  };

  return (
    <AdminContext.Provider
      value={{
        appointments,
        setAppointments,
        getAppointmentsByDate,
        selectedDate,
        setSelectedDate,
        updateAppointment, 
        deleteAppointment,
        successMessage,
        setSuccessMessage,
      }}
    >
      {children}
    </AdminContext.Provider>
  );
};

export const useAdmin = () => {
  return useContext(AdminContext);
};
