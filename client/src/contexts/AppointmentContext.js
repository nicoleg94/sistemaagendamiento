import React from "react";
import { createContext, useContext, useState, useEffect } from "react";
import axios from "axios";

const AppointmentContext = createContext();

export const AppointmentProvider = ({ children }) => {
  const [selectedDate, setSelectedDate] = useState(Date.now());
  const [selectedHour, setSelectedHour] = useState(null);
  const [patientData, setPatientData] = useState({
    idCard: "",
    name: "",
    age: "",
    representativeName: "",
    contactPhone: "",
    contactEmail: "",
    contactEmail2: "",
  });

  const getAvailableHours = async (date) => {
    try {
      const response = await axios.get(`http://localhost:3001/appointment/available_hours?date=${date}`);
      console.log("Horas disponibles:", response.data.available_hours);
      return response.data.available_hours;
    } catch (error) {
      console.error('Error al obtener las horas disponibles:', error);
      return [];
    }
  };

  const sendAppointmentDataToBackend = async () => {
    const dataToSend = {
      selectedDate,
      selectedHour,
      ...patientData,
    };

    try {
      const response = await axios.post("http://localhost:3001/appointment", dataToSend);
      setPatientData({
        idCard: "",
        name: "",
        age: "",
        representativeName: "",
        contactPhone: "",
        contactEmail: "",
        contactEmail2: "",
      });
    } catch (error) {
      console.error("Error al agendar la cita:", error);
    }
  };

  return (
    <AppointmentContext.Provider
      value={{
        selectedDate,
        setSelectedDate,
        selectedHour,
        setSelectedHour,
        patientData,
        setPatientData,
        sendAppointmentDataToBackend,
        getAvailableHours,
      }}
    >
      {children}
    </AppointmentContext.Provider>
  );
};

export const useAppointment = () => {
  return useContext(AppointmentContext);
};
