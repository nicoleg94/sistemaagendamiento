class CreatePatients < ActiveRecord::Migration[7.1]
  def change
    create_table :patients do |t|
      t.string :idCard
      t.string :name
      t.integer :age
      t.string :representativeName
      t.string :contactPhone
      t.string :contactEmail
      t.string :contactEmail2

      t.timestamps
    end
  end
end